package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    WebDriver driver;

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By CART_TITLE = By.className("entry-title");
    private final By CART_MESSAGE = By.className("cart-empty");
    private final By CART_VALUE = By.className("cart-value");

    public String getCartTitle() {
        return driver.findElement(CART_TITLE).getText();
    }

    public String getCartMessage() {
        return driver.findElement(CART_MESSAGE).getText();
    }

    public int getCartValue(){
        return Integer.parseInt(driver.findElement(CART_VALUE).getText());
    }

}
