package search;

import org.junit.Assert;
import org.junit.Test;
import page_Objects.HomeShopPage;
import page_Objects.SearchResultPage;
import utils.BaseTestClass;

public class SearchTests extends BaseTestClass {

    @Test
    public void checkSearchResults(){
        HomeShopPage homeShopPage  = new HomeShopPage(driver);
        homeShopPage.clickOnSearch();
        SearchResultPage searchResultPage=homeShopPage.typeOnSearchField("Watch");
        Assert.assertTrue(searchResultPage.checkResultsContainSearch("Watch"));

    }

}
