package cart;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import page_Objects.CartPage;
import page_Objects.CategoryPage;
import page_Objects.HomeShopPage;
import utils.BaseTestClass;

public class CartTests extends BaseTestClass {

    @Test
    public void checkEmptyCart() {
        //click on Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();
        //check cart page title
        Assert.assertEquals("CART", driver.findElement(By.className("entry-title")).getText());
        //check Cart page  message
        Assert.assertEquals("Your cart is currently empty.", driver.findElement(By.className("cart-empty")).getText());
    }

    @Test
    public void checkEmptyCartWithPageObject() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        CartPage cartPage = homeShopPage.clickOnCart();

        Assert.assertEquals("CART", cartPage.getCartTitle());
        Assert.assertEquals("Your cart is currently empty.", cartPage.getCartMessage());
    }

    @Test
    public void checkItemAddedToCart() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage categoryPage = homeShopPage.clickOnMenCollection();
        categoryPage.addToCart();
        CartPage cartPage= new CartPage(driver);
        Assert.assertEquals("1",cartPage.getCartValue());



    }

}
