package category;

import org.junit.Assert;
import org.junit.Test;
import page_Objects.CategoryPage;
import page_Objects.HomeShopPage;
import utils.BaseTestClass;

public class CategoryTests extends BaseTestClass {

    @Test
    public void checkMenTitle() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage menCollection = homeShopPage.clickOnMenCollection();
        Assert.assertEquals("Men Collection", menCollection.getCategoryTitle());
    }

}
